class SafeMail {

    constructor(redirectToLoginPageIfSessionInvalid = false) {
        console.log("[SafeMail][info] safemail cunstructor");
        // cunstruct xmlhttp
        this.xhttp = new XMLHttpRequest();
        this.host =  window.location.href.substring(0, window.location.href.lastIndexOf("/"));

        // check if login already exists
        if (this.getCookie("session") !== "" && document.cookie.includes("session")) {
            // there is a stored session cookie
            var session = this.getCookie("session")
            console.log("[SafeMain][info] found old session")
            var url = window.location.pathname;
            if (url.substring(url.lastIndexOf('/') + 1) === "index.html") {
                window.location.href = "./main.html"
            }
        }
        else if (redirectToLoginPageIfSessionInvalid) {
            window.location.href = "./index.html"
        }
    }

    login(username, password, permanent, errorFieldId) {
        var host = this.host
        this.sendRequest(1, { "username": username, "password": password }, function (xhttp) {
            if (xhttp.status === 200) {
                console.log(xhttp.responseText);
                var ret = JSON.parse(xhttp.responseText);
                if (ret['header'] === 200) {
                    console.log("[SafeMail][info] login success")
                    document.getElementById(errorFieldId).style.display = "none"
                    if (permanent) {
                        console.log("[SafeMail][info] creating permanent cookie")
                        var expires = new Date(new Date().getTime() + parseInt(1000) * 365 * 1000 * 60 * 60 * 24);
                        document.cookie = "session=" + ret['data'] + "; expires=" + expires.toGMTString() + ";"
                    }
                    else {
                        document.cookie = "session=" + ret['data']
                    }

                    window.location.href = "./main.html"//host + "frontend/main.html";
                    return true
                }
                else {
                    console.log("[SafeMail][info] login fail")
                    document.getElementById(errorFieldId).innerHTML = "Your password or Username is invalid!"
                    document.getElementById(errorFieldId).style.display = "block"
                    return false
                }
            }
            else {
                document.getElementById(errorFieldId).innerHTML = "No connection to Server"
                document.getElementById(errorFieldId).style.display = "block"
            }
        })
    }

    register(username, email, password, errorFieldId) {
        var host = this.host
        this.sendRequest(2, { "username": username, "password": password, "email": email }, function (xhttp) {
            if (xhttp.status === 200) {
                console.log(xhttp.responseText);
                var ret = JSON.parse(xhttp.responseText);
                if (ret['header'] === 200) {
                    console.log("[SafeMail][info] register success")
                    document.getElementById(errorFieldId).className = "alert alert-success";
                    document.getElementById(errorFieldId).innerHTML = "Your Account has been created"
                    document.getElementById(errorFieldId).style.display = "block"
                    return true
                }
                else {
                    console.log("[SafeMail][info] register fail")
                    document.getElementById(errorFieldId).className = "alert alert-danger";
                    document.getElementById(errorFieldId).innerHTML = "Your username or email is already in use!"
                    document.getElementById(errorFieldId).style.display = "block"
                    return false
                }
            }
            else {
                document.getElementById(errorFieldId).className = "alert alert-danger";
                document.getElementById(errorFieldId).innerHTML = "No connection to Server"
                document.getElementById(errorFieldId).style.display = "block"
            }
        })
    }

    endSession() {
        document.cookie = "session=;"
        window.location.href = "./index.html"
    }

    getAllMailAddresses(tableId) {
        var thisObject = this
        this.sendRequest(200, this.getCookie("session"), function (xhttp) {
            if (xhttp.status === 200) {
                console.log(xhttp.responseText);
                var ret = JSON.parse(xhttp.responseText);
                if (ret['header'] === 200) {
                    console.log("[SafeMail][info] get mail addresses success")
                    var mailAddresses = ret.data
                    document.getElementById(tableId).innerHTML = ""
                    for (var i = 0; i < mailAddresses.length; i++) {
                        var currentHTML = document.getElementById(tableId).innerHTML
                        document.getElementById(tableId).innerHTML = currentHTML +
                            "<tr class=\"success\"><td>" + DOMPurify.sanitize(mailAddresses[i]["name"]) + "</td><td><a target='blank' href=http://" + DOMPurify.sanitize(mailAddresses[i]["domain"]) + ">" + DOMPurify.sanitize(mailAddresses[i]["domain"]) + "</a></td><td>" + DOMPurify.sanitize(mailAddresses[i]["mailAddress"]) + "</td><td><button type=\"button\" class=\"btn btn-info btn-lg\"onclick=\"window.location.href='inbox.html?mailAddress=" + DOMPurify.sanitize(mailAddresses[i]["id"]) + "'\">⮩</button></td></tr>";
                    }
                    return ret.data
                }
                else if (ret['header'] === 401) {
                    thisObject.endSession()
                }
                else {
                    return []
                }
            }
        })
    }

    addMailAddress(domain, name) {
        var host = this.host
        var thisObject = this
        this.sendRequest(100, { "session": this.getCookie("session"), "domain": domain, "name": name }, function (xhttp) {
            if (xhttp.status === 200) {
                console.log(xhttp.responseText);
                var ret = JSON.parse(xhttp.responseText);
                if (ret['header'] === 200) {
                    console.log("[SafeMail][info] add mail success")
                    $('#myModal').modal('hide');
                    thisObject.getAllMailAddresses("mailAddressesTable")
                    return true
                }
                else {
                    console.log("[SafeMail][info] add mail failure")
                    return false
                }
            }
        })
    }

    getAllMails(mailAddress, tableId) {
        var thisObject = this
        this.sendRequest(201, { "session": this.getCookie("session"), "mailAddress": mailAddress }, function (xhttp) {
            if (xhttp.status === 200) {
                console.log(xhttp.responseText);
                var ret = JSON.parse(xhttp.responseText);
                if (ret['header'] === 200) {
                    console.log("[SafeMail][info] get mails success")
                    var mails = ret.data
                    document.getElementById(tableId).innerHTML = ""
                    for (var i = 0; i < mails.length; i++) {
                        var currentHTML = document.getElementById(tableId).innerHTML
                        document.getElementById(tableId).innerHTML = currentHTML + `<tr>
                            <span class=\"caret\"></span></button>
                            <td>` + DOMPurify.sanitize(mails[i]["sender"]) + `</td>
                            <td>` + DOMPurify.sanitize(mails[i]["subject"]) + `</td>
                            <td>` + DOMPurify.sanitize(mails[i]["timestamp"]) + `</td>
                            <td>  <button type="button" class="btn btn-info btn-lg" data-toggle="modal"  data-target="#topcock` + i + `">▲</button>
                            <!-- Modal -->
                            <div id="topcock` + i + `" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                    <p>` + DOMPurify.sanitize(mails[i]["body"]) + `</p>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                                </div></td>
                        </tr>`

                    }
                    return ret.data
                }
                else if (ret['header'] === 401) {
                    thisObject.endSession()
                }
                else {
                    return []
                }
            }
        })
    }

    sendRequest(header, data, processingFunction) {
        this.xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                // Typical action to be performed when the document is ready:
                processingFunction(this);
            }
        };

        var jsonCommand = JSON.stringify({ "header": header, "data": data })
        var fullUrl = this.host + "/backend/json.php?command=" + jsonCommand

        console.log("starting request; URL is: " + fullUrl)

        this.xhttp.open("GET", fullUrl, true);
        this.xhttp.send();
    }

    getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }

    findGetParameter(parameterName) {
        var result = null,
            tmp = [];
        location.search
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            });
        return result;
    }
}
