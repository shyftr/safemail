# //  SAFE  ||  E-MAIL  //

This is Safemail (good stuff) to manage your online accounts email addresses.

## Information
It's a httf project.

## Installation

#### Installation on Linux (Ubuntu / Debian / Linux Mint / Kali)

1. Install the required packages
```
sudo apt install git apache2 mysql-server mysql-client php libapache2-mod-php php-mysql php-mailparse -y
```
2. Prepare the working directory and download SafeMail
```
cd /var/www/html/

git clone https://gitlab.com/mindcrew-dev/safemail.git
```
3. Create the database
```
sudo mysql -u root

CREATE DATABASE safemail;
```
4. now create the database user, if you change the username, password or database name, don't forget to change that data in the config.php file!

```
GRANT ALL ON safemail.* TO 'safemail'@'localhost' IDENTIFIED BY 'root';

FLUSH PRIVILEGES;

exit;
```
5. Prepare database for usage (create Tables and Keys)
```
cd /var/www/html/safemail/backend

sudo mysql -u root safemail < safemail.sql
```
Your SafeMail instance can now be reached under: <your_ip>/safemail

Configure the mail server (Postfix)

1. Install Postfix

```
sudo DEBIAN_PRIORITY=low apt install postfix
```
Options to choose:  
General type of mail configuration: Internet Site  
System mail name: domain of your mail server (e.g. safemail.example.com)  
Root and postmaster mail recipient: safemail (user who will recieve mails to root@ or postmaster@)  
Other destinations to accept mail for (blank for none): safemail.itsblue.de, localhost  
Force synchronous updates on mail queue?: no  
Local networks: 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128  
Mailbox size limit (bytes): 0  
Local address extension character: +  
Internet protocols to use: all  
  
Configure destination for mails  
```
sudo postconf -e 'home_mailbox= Maildir/'
```
set the location of the virtual_alias_maps table
```
sudo postconf -e 'virtual_alias_maps= hash:/etc/postfix/virtual'
```
create and configure virtual maps file
```
sudo nano /etc/postfix/virtual
```
Add the following to the file:
```
@<yourdomain.com> <your user for incoming mail>
```
In our case:
```
@safemail.itsblue.de safemail
```
Apply config
```
sudo postmap /etc/postfix/virtual
```
restart postfix
```
sudo systemctl restart postfix
```

## How To Use
```
$ echo "I don't know how it works!¡?¿"
```

## Built With

* [php](https://php.net//) - Programming language for backend (oh no)
* [HTML](https://www.w3schools.com/html/) - foof
* [CSS](https://www.w3.org/Style/CSS/) - webshit pretty normal
* [Bootstrap](https://getbootstrap.com/) - css stuff y know

## Authors

* **Sascha S.** - [Mv0sKff](https://gitlab.com/Mv0sKff)
* **Maris B.** - [TheProgramming_M](https://gitlab.com/TheProgramming_M)
* **Dorian Z.** - [dozedler](https://gitlab.com/dozedler)
* **Max G.** - [SomeThink](https://gitlab.com/SomeThink)
* **Silas S.** - [SILAS.ICH.SCHWOER](https://gitlab.com/SILAS.ICH.SCHWOER)

## License

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
