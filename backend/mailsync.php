<?php

/**
 * Safemail
 *
 * PHP version 7.2
 *
 * @category Tools
 * @package  SafeMail
 * @author   MindCrew <mc@trshmail.com>
 * @license  GPLV3 gpl.com
 * @link     safemail.itsblue.de
 */

require_once __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';

$mailDir = '/home/safemail/Maildir/new';

$parser = new PhpMimeMailParser\Parser();

$con = mysqli_connect($config['dbhost'], $config['dbuser'], $config['dbpassword'], $config['dbname']);
if (!$con) {
    echo "<h1>Fatal internal Error! :-/</h1>";
    echo "Error connecting to database: " . mysqli_connect_error();
    exit();
}

$dir = new DirectoryIterator($mailDir);
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) {
        //var_dump($fileinfo->getFilename());
        $path = $fileinfo->getPath() . "/" . $fileinfo->getFilename();

        // 1. Specify a file path (string)
        $parser->setPath($path);

        $recievers = $parser->getAddresses('to');
        
        $body = $parser->getMessageBody('html');
        $subject = $parser->getHeader('subject');
        $date = strtotime($parser->getHeader('Date'));
        $sender = $parser->getHeader('from');
        
        //print_r($recievers);
        $recieverIds = array();
        
        for ($i = 0; $i < count($recievers); $i++) {
            $sql = "SELECT * FROM `addresses`
            WHERE`mail`=\"" . $con->real_escape_string($recievers[$i]["address"]) . "\"";
            $result = $con->query($sql);
        
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    array_push($recieverIds, $row["id"]);
                }
            }
        }
        
        for ($i = 0; $i < count($recieverIds); $i++) {
            $sql = 'INSERT INTO mails (sender, addressId, subject, timestamp, body)
                    VALUES("' . $con->real_escape_string($sender) . '", '.$recieverIds[$i].', "'.$con->real_escape_string($subject).'", "'.$con->real_escape_string($date).'", "'.$con->real_escape_string($body).'")';
                    //echo $sql;
            if (!$con->query($sql)) {
                die("error inserting");
            }
        }

        unlink($path);
    }
}
?>