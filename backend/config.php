<?php
/**
 * Safemail
 *
 * PHP version 7.2
 *
 * @category Tools
 * @package  SafeMail
 * @author   MindCrew <mc@trshmail.com>
 * @license  GPLV3 gpl.com
 * @link     safemail.itsblue.de
 */

$config['dbhost'] = 'localhost';
$config['dbname'] = 'safemail';
$config['dbuser'] = 'safemail';
$config['dbpassword'] = 'root';

?>